

function checkFields() {
		if (document.getElementById('fname').value == "") {
			alert("Please input your First Name");
			return false;
		}

		if (document.getElementById('lname').value == "") {
			alert("Please input your Last Name");
			return false;
		}

		if (document.getElementById('email').value == "") {
			alert("Please input your Email Address");
			return false;
		}

		if ( document.getElementById('pword').value.length < 7) {
			alert("Password is required. Password must be atlease 8 characters");
			return false;

		}

		if (document.getElementById('pword').value !== document.getElementById('confirmPword').value) {
			alert("Password did not match");
			return false;

		}

		else {
			alert("Thanks for logging your information");
		}

}
